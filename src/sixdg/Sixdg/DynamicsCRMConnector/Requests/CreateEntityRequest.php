<?php
namespace Sixdg\DynamicsCRMConnector\Requests;

use Sixdg\DynamicsCRMConnector\Builders\RequestBuilder;
use Sixdg\DynamicsCRMConnector\Requests\AbstractSoapRequest;

/**
 * Class CreateEntityRequest
 *
 * @package Sixdg\DynamicsCRMConnector\Requests
 */
class CreateEntityRequest extends AbstractSoapRequest
{
    protected $action = 'http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Create';
    protected $to = 'XRMServices/2011/Organization.svc';

    protected $entity;
    /**
     * @var EntityToDomConverter
     */
    protected $entityToDomConverter;

    /**
     * @param RequestBuilder $requestBuilder
     */
    public function __construct(RequestBuilder $requestBuilder)
    {
        $this->entity = $requestBuilder->getEntity();
        $this->entityToDomConverter = $requestBuilder->getEntityToDomConverter();
        $this->securityToken = $requestBuilder->getSecurityToken();

        parent::__construct($requestBuilder);
    }

    /**
     * @return \DOMNode
     */
    protected function getBody()
    {
        $body = $this->domHelper->createNewDomDocument();
        $bodyNode = $body->appendChild($this->domHelper->createElement('s:Body'));
        $bodyNode->appendChild($this->getBodyContents($body));

        return $bodyNode;
    }

    protected function getBodyContents($body)
    {
        $createNode = $this->domHelper->createElementNS($this->serviceNS, 'Create');
        $this->setSchemaInstance($createNode);
        $createNode->appendChild($this->getEntityNode($body));

        return $createNode;
    }

    protected function setSchemaInstance($node)
    {
        $node->setAttributeNS(
            'http://www.w3.org/2000/xmlns/',
            'xmlns:i',
            'http://www.w3.org/2001/XMLSchema-instance'
        );
    }

    protected function getEntityNode($body)
    {
        $entityNode = $this->entityToDomConverter->convert($this->entity);

        return $body->importNode($entityNode, true);
    }
}
