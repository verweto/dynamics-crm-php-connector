<?php
namespace Sixdg\DynamicsCRMConnector\Models;

/**
 * Class EntityMetadataRegistryItem
 *
 * @package Sixdg\DynamicsCRMConnector\Models
 */
class EntityMetadataRegistryItem
{
    private $logicalName;
    private $label;
    private $description;
    private $isCustomAttribute;
    private $isPrimaryId;
    private $isPrimaryName;
    private $attributeType;
    private $isLookup;
    private $lookupTypes;
    private $isValidForCreate;
    private $isValidForUpdate;
    private $isValidForRead;
    private $requiredLevel;
    private $attributeOf;
    private $optionSet;

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getIsPrimaryId()
    {
        return $this->isPrimaryId;
    }

    /**
     * @param mixed $isPrimaryId
     */
    public function setIsPrimaryId($isPrimaryId)
    {
        $this->isPrimaryId = $isPrimaryId;
    }

    /**
     * @return mixed
     */
    public function getIsPrimaryName()
    {
        return $this->isPrimaryName;
    }

    /**
     * @param mixed $isPrimaryName
     */
    public function setIsPrimaryName($isPrimaryName)
    {
        $this->isPrimaryName = $isPrimaryName;
    }

    /**
     * @return mixed
     */
    public function getIsLookup()
    {
        return $this->isLookup;
    }

    /**
     * @param mixed $isLookup
     */
    public function setIsLookup($isLookup)
    {
        $this->isLookup = $isLookup;
    }

    /**
     * @return mixed
     */
    public function getLookupTypes()
    {
        return $this->lookupTypes;
    }

    /**
     * @param mixed $lookupTypes
     */
    public function setLookupTypes($lookupTypes)
    {
        $this->lookupTypes = $lookupTypes;
    }

    /**
     * @return mixed
     */
    public function getRequiredLevel()
    {
        return $this->requiredLevel;
    }

    /**
     * @param mixed $requiredLevel
     */
    public function setRequiredLevel($requiredLevel)
    {
        $this->requiredLevel = $requiredLevel;
    }

    /**
     * @return mixed
     */
    public function getAttributeOf()
    {
        return $this->attributeOf;
    }

    /**
     * @param mixed $attributeOf
     */
    public function setAttributeOf($attributeOf)
    {
        $this->attributeOf = $attributeOf;
    }

    /**
     * @return mixed
     */
    public function getOptionSet()
    {
        return $this->optionSet;
    }

    /**
     * @param mixed $optionSet
     */
    public function setOptionSet($optionSet)
    {
        $this->optionSet = $optionSet;
    }

    /**
     * @return mixed
     */
    public function getAttributeType()
    {
        return $this->attributeType;
    }

    /**
     * @param mixed $attributeType
     */
    public function setAttributeType($attributeType)
    {
        $this->attributeType = $attributeType;
    }

    /**
     * @return mixed
     */
    public function getIsValidForCreate()
    {
        return $this->isValidForCreate;
    }

    /**
     * @param mixed $isValidForCreate
     */
    public function setIsValidForCreate($isValidForCreate)
    {
        $this->isValidForCreate = $isValidForCreate;
    }

    /**
     * @return mixed
     */
    public function getIsValidForUpdate()
    {
        return $this->isValidForUpdate;
    }

    /**
     * @param mixed $isValidForUpdate
     */
    public function setIsValidForUpdate($isValidForUpdate)
    {
        $this->isValidForUpdate = $isValidForUpdate;
    }

    /**
     * @return mixed
     */
    public function getIsValidForRead()
    {
        return $this->isValidForRead;
    }

    /**
     * @param mixed $isValidForRead
     */
    public function setIsValidForRead($isValidForRead)
    {
        $this->isValidForRead = $isValidForRead;
    }

    /**
     * @return mixed
     */
    public function getIsCustomAttribute()
    {
        return $this->isCustomAttribute;
    }

    /**
     * @param mixed $isCustomAttribute
     */
    public function setIsCustomAttribute($isCustomAttribute)
    {
        $this->isCustomAttribute = $isCustomAttribute;
    }

    /**
     * @return mixed
     */
    public function getLogicalName()
    {
        return $this->logicalName;
    }

    /**
     * @param mixed $logicalName
     */
    public function setLogicalName($logicalName)
    {
        $this->logicalName = $logicalName;
    }
}
