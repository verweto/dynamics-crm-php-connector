<?php
use Sixdg\DynamicsCRMConnector\Services\SecurityService;
/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 04/07/13
 * Time: 15:57
 */

/**
 * Class SecurityServiceTest
 */
class SecurityServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var SecurityService
     */
    protected $securityService;

    public function setUp()
    {
        $loginRequest = \Mockery::mock('\Sixdg\DynamicsCRMConnector\Requests\LoginRequest');
        $loginRequest->shouldReceive('getXML')->andReturn(
            file_get_contents(__DIR__ . '/../Requests/Fixtures/LoginXML.xml')
        );
        $loginRequest->shouldReceive('getTo')->passthru();

        $requestBuilder = \Mockery::mock('\Sixdg\DynamicsCRMConnector\Builders\RequestBuilder');
        $requestBuilder->shouldReceive('reset')->andReturn($requestBuilder);
        $requestBuilder->shouldReceive('setCrm')->passthru();
        $requestBuilder->shouldReceive('setServer')->passthru();
        $requestBuilder->shouldReceive('setDiscoveryUrl')->passthru();
        $requestBuilder->shouldReceive('setUsername')->passthru();
        $requestBuilder->shouldReceive('setPassword')->passthru();
        $requestBuilder->shouldReceive('getRequest')->andReturn($loginRequest);

        $soapRequest = \Mockery::mock('\Sixdg\DynamicsCRMConnector\Components\Soap\SoapRequester');
        $xml = file_get_contents(__DIR__ . '/Fixtures/securityXML.xml');
        $xml = str_replace(array("\n", "\t", "\r"), '', $xml);
        $xml = preg_replace('/>\s+</', "><", $xml);
        $soapRequest->shouldReceive('sendRequest')->andReturn($xml);

        $this->securityService = new SecurityService($requestBuilder, $soapRequest);
    }

    public function testLogin()
    {
        $startOfSecurityToken = '<xenc:EncryptedData';

        $config = $GLOBALS['config'];

        $securityToken = $this->securityService->login(
            $config['adfs'],
            $config['crm'],
            $config['discoveryUrl'],
            $config['username'],
            $config['password']
        );

        $this->assertTrue(is_array($securityToken));
        $this->assertEquals('14LsD1aRCt5UIJ4MDeXlOMVfG8UV/W5tLa1AOG3LEnA=', $securityToken['binarySecret']);
        $this->assertEquals('_168d3836-d7cf-41e9-b076-f49970160328', $securityToken['keyIdentifier']);
        $this->assertTrue(
            !strncmp($securityToken['securityToken'], $startOfSecurityToken, strlen($startOfSecurityToken))
        );
    }
}
