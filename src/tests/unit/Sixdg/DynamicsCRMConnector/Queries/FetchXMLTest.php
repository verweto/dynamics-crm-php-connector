<?php

use Sixdg\DynamicsCRMConnector\Test\BaseTest;
use Sixdg\DynamicsCRMConnector\Queries\FetchXML;

/**
 * @author Leandro Miranda
 */
class FetchXMLTest extends BaseTest
{

    protected $query;

    public function setUp()
    {
        $this->query = new FetchXML();
        $this->query->setEntityName('contact');
        $this->query->addAnd([
            'attribute' => 'lastname',
            'operator' => 'eq',
            'value' => 'test'
        ]);
        $this->query->addOr([
            'attribute' => 'lastname',
            'operator' => 'ne',
            'value' => 'test'
        ]);
    }

    /**
     *
     */
    public function testGetFetchExpression()
    {
        $xml = $this->query->getFetchExpression();
        $expected = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true"><entity name="contact"><filter type="and"><condition attribute="lastname" operator="eq" value="test"/></filter><filter type="or"><condition attribute="lastname" operator="ne" value="test"/></filter></entity></fetch>';
        $this->assertEquals($xml, $expected);
    }
}
